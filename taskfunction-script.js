alert(`Selamat Datang di Game Tebak Angka!
Kamu akan diminta untuk menebak angka 1 - 3 dan kamu akan bermain dalam 5 ronde.
Player dengan tebakan terbanyak akan menjadi pemenangnya.

 Selamat Bermain!!`);

let scoreP1 = 0;
let scoreP2 = 0;
let ronde = 1;
let stop = true;

while (stop) {
  let player1 = parseInt(prompt(`Player 1 : Silakan Masukkan Angka!`));
  let player2 = parseInt(prompt(`Player 2 : Silakan Masukkan Angka!`));

  let random = getRandom();
  let ulang = validasi(player1, player2);
  if (!ulang) {
    stop = window.confirm(`Ulang?`);
  } else {
    if (player1 !== random && player2 !== random) {
      alert(`Tidak ada yang benar. Hasil Seri!`);
    } else {
      if (player1 === random) {
        alert(`Player 1 Win!`);
        scoreP1++;
      }
      if (player2 === random) {
        alert(`Player 2 Win!`);
        scoreP2++;
      }
    }

    alert(`Nilai yang dicari: ${random}
  -----------------------------------
  - Player 1: ${scoreP1}
  - Player 2: ${scoreP2}`);

    ronde++;
    if (ronde <= 5) {
      stop = window.confirm(`Ronde` + ronde + `?`);
    } else {
      if (scoreP1 > scoreP2) {
        alert(`Good Jobs Player 1!`);
        stop = false;
      } else if (skorP1 < scoreP2) {
        alert(`Good Jobs Player 2!`);
        stop = false;
      } else {
        alert(`Pertarungan yang Sengit`);
        ronde = 1;
        scoreP1 = 0;
        scoreP2 = 0;
        stop = window.confirm(`Main kembali?`);
      }
    }
  }
}

function validasi(p1, p2) {
  if (p1 === p2) {
    alert(`Tebakan tidak boleh sama!`);
    return false;
  }
  if (p1 < 1 || p2 < 1) {
    alert(`Tebakan tidak boleh lebih kecil dari 1!`);
    return false;
  }
  if (p1 > 3 || p2 > 3) {
    alert(`Tebakan tidak boleh lebih besar dari 3!`);
    return false;
  }
  if (isNaN(p1) || isNaN(p2)) {
    alert(`Salah satu player belum menebak.`);
    return false;
  }

  return true;
}

function getRandom() {
  const range = [1, 2, 3];
  let norandom = true;
  while (norandom) {
    let random = Math.floor(Math.random() * 10);
    let ketemu = range.find((r) => r === random);
    if (ketemu) {
      norandom = false;
      return random;
    }
  }
}

console.log(getRandom());
